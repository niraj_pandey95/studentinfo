﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InfoApplication.Models;

namespace InfoApplication.Controllers
{
    public class InformationController : ApiController
    {
        public IHttpActionResult GetAllStudnetinfo()
        {
            IList<Information> informations = null;
            using (var x = new InfoApiDemoEntities())
            {
                informations = x.Studentinfoes
                                         .Select(c => new Information()
                                         {
                                             Id = c.Id,
                                             Name = c.Name,
                                             Address = c.Address,
                                             Roll = (int)c.Roll,

                                         }).ToList<Information>();
            }

                if (informations.Count == 0)
                    return NotFound();

                return Ok(informations);
            }
        public IHttpActionResult PostNewStudentinfo(Information information1)
            {
                if (!ModelState.IsValid)
                return BadRequest("Not valid, Please recheck");

                using(var x = new InfoApiDemoEntities())
            {
                x.Studentinfoes.Add(new Studentinfo()
                {
                    Id = information1.Id,
                    Name = information1.Name,
                    Address = information1.Address,
                    Roll = information1.Roll,
                });

                x.SaveChanges();
            }
            return Ok();
            }
        public IHttpActionResult PutStudentinfo(Information information1)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid, Please recheck");

            using(var x = new InfoApiDemoEntities())
            {
                var checkExistingStudentinfo = x.Studentinfoes.Where(c => c.Id == information1.Id)
                                                                                        .FirstOrDefault<Studentinfo>();
                if (checkExistingStudentinfo != null)
                {
                    checkExistingStudentinfo.Id = information1.Id;
                    checkExistingStudentinfo.Name = information1.Name;
                    checkExistingStudentinfo.Address = information1.Address;
                    checkExistingStudentinfo.Roll = information1.Roll;

                    x.SaveChanges();
                }
                else
                    return NotFound(); 
            }
            return Ok();
        }
        public IHttpActionResult Delete(int Id)
        {
            if (Id <= 0)
                return BadRequest("Enter valid Id");
            using(var x = new InfoApiDemoEntities())
            {
                var information = x.Studentinfoes.Where(c => c.Id == Id).FirstOrDefault();
                x.Entry(information).State = System.Data.Entity.EntityState.Deleted;
                x.SaveChanges();
            }

            return Ok();
        }
        }
    }

