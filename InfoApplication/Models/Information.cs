﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoApplication.Models
{
    public class Information
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; } 
        public int Roll { get; set; }
    }
}